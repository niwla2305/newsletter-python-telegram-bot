from telegram.ext import *
from telegram import *

reply_keyboard = [['/Kategorien', '/Stop']]
reply_markup2 = ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=False, resize_keyboard=True)


def start(bot, update):
    update.message.reply_text("*Willkommen im FFF-Newsletter!*\nHier gibt es alle aktuellen Infos zu Fridays for Future.\nUnter Kategorien kannst du auswählen, welche Nachrichten du erhalten möchtest. Wenn du keine Nachrichten mehr empfangen möchtest, drücke auf Stopp.", reply_markup=reply_markup2, parse_mode="Markdown")


def kategorien(bot, update):
    keyboard = [[InlineKeyboardButton("Weekly ✅", callback_data='1'),
                 InlineKeyboardButton("News ✅", callback_data='2')],
                [InlineKeyboardButton("Presse ✅", callback_data='3'),
                InlineKeyboardButton("Bot ✅", callback_data='3')]]

    reply_markup = InlineKeyboardMarkup(keyboard)
    update.message.reply_text("*Kategorien*\nDu kannst hier auswählen, aus welchen Kategorien du Nachrichten erhalten möchtest.\n\n📆 Weekly\n📂 Wöchentliche Zusammenfassung\n✉️ eine Nachricht pro Woche\n\n🗞 News\n📂 Aktuelle Neuigkeiten\n✉️ seltener, max. eine Nachricht pro Tag\n\n🎙 Presse\n📂 Presseinformationen\n✉️ seltener, je nach Tag und Aktivität\n\n🤖 Bot\n📂 Neuigkeiten zum Bot und zu neuen Funktionen\n✉️ selten", reply_markup=reply_markup,parse_mode="Markdown")
    print("Kategorien ausgeführt")

def help(bot, update):
    update.message.reply_text("Dieser Bot sendet den FridaysForFuture Germany Newsletter. Um ihn zu stioppen: /stop",
                              parse_mode="Markdown")