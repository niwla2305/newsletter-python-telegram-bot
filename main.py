from telegram.ext import *
from telegram import *
import config
import functions


updater = Updater(config.token)

updater.dispatcher.add_handler(CommandHandler('start', functions.start))
updater.dispatcher.add_handler(CommandHandler('s', functions.start))

updater.dispatcher.add_handler(CommandHandler('Kategorien', functions.kategorien))
updater.dispatcher.add_handler(CommandHandler('kategorien', functions.kategorien))
updater.dispatcher.add_handler(CommandHandler('k', functions.kategorien))

updater.dispatcher.add_handler(CommandHandler('help', functions.help))


updater.start_polling()
updater.idle()
